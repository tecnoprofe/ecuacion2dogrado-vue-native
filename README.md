# APP MOVIL 
## Ecuación de 2do grado en Vue Native

Este es un ejercicio de creación de una aplicación de un ejercicio matemático de la ecuación de 2do grado.

## INSTALACIÓN

para este proceso debe contar antes con la aplicación [laragon](https://razaoinfo.dl.sourceforge.net/project/laragon/releases/4.0/laragon-full.exe)

### Paso 1. Descargar el proyecto

```bash
$ git clone https://gitlab.com/tecnoprofe/ecuacion2dogrado-vue-native.git
```

### Paso 2. Instalar dependencias JS

```bash
$ cd ecuacion2dogrado-vue-native
$ npm install
```
### Paso 3. Ejecutar

```bash
$ npm start.
```
## REQUERIMIENTOS MÍNIMOS

Para este proyecto se recomienda tener instalado laragon, con:
### Node 
Si no cuenta con esta verisón, puede descargar desde [aquí](https://nodejs.org/dist/v12.13.1/node-v12.13.1-win-x64.zip)
```bash
$ node --version
v12.13.1
```
### npm
```bash
$ npm --version
6.12.1
```
### npm
```bash
$ expo-cli --version
4.3.0
```
### Vue-Native
```bash
$ vue-native --version
0.2.0
```
# Acerca del autor
Jaime Zambrana Chacón es un profesor universitario (Grado - Posgrado), investigador y desarrollador web aplicando IA. 
Visita mis redes sociales [Facebook](https://www.facebook.com/zambranachaconjaime/)

